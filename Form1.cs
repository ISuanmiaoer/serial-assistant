﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serial_Assisitant
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 窗口初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames(); // 获取电脑上可用的串口号
            comboBox1.Items.AddRange(ports); // 给comboBox1添加数据
            comboBox1.SelectedIndex = comboBox1.Items.Count > 0 ? 0 : -1; // 如果comboBox1有数据，显示第0个
            comboBox2.Text = "115200"; /*默认波特率*/
            comboBox3.Text = "8"; /*默认数据位*/
            comboBox4.Text = "None"; /*默认校验位*/
            comboBox5.Text = "1"; /*默认停止位*/
        }
        /// <summary>
        /// 打开串口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                // 根据当前串口属性判断串口是否打开
                if(serialPort1.IsOpen)
                {   // 如果串口处于打开状态，则关闭当前串口
                    serialPort1.Close();
                    button2.Text = "打开串口";
                    button2.BackColor = Color.SpringGreen;
                    ComboBoxEnabled(true);//comboBox可选择
                    //接收区和发送区都清空
                    textBox1.Clear();
                    textBox2.Clear();
                }
                else
                {// 如果当前没有串口处于打开状态
                    ComboBoxEnabled(false);//设置comboBox不可选择
                    serialPort1.PortName = comboBox1.Text;//设置串口号
                    serialPort1.BaudRate = Convert.ToInt32(comboBox2.Text);//设置波特率
                    serialPort1.DataBits = Convert.ToInt32(comboBox3.Text);//设置数据位
                    serialPort1.Parity = SetParity(comboBox4.Text);//设置校验位
                    serialPort1.StopBits = SetStopBits(comboBox5.Text);//设置停止位
                    serialPort1.Open();//打开串口
                    button2.Text = "关闭串口";
                    button2.BackColor = Color.Red;
                }
            }catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                // 重新创建一个新的对象，之前的串口不可用
                serialPort1 = new SerialPort();
                // 刷新COM口选项
                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(SerialPort.GetPortNames());
                comboBox1.SelectedIndex = comboBox1.Items.Count > 0 ? 0 : -1; // 如果comboBox1有数据，显示第0个
                button2.Text = "打开串口";
                button2.BackColor = Color.SpringGreen;
                ComboBoxEnabled(true);
            }
        }
        /// <summary>
        /// 设置停止位方法
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private StopBits SetStopBits(string text)
        {
            if (text.Equals("1"))
            {
                return StopBits.One;
            }else if (text.Equals("1.5"))
            {
                return StopBits.OnePointFive;
            }else if (text.Equals("2"))
            {
                return StopBits.Two;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// 设置校验位方法
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private Parity SetParity(string text)
        {
            if(text.Equals("None"))
            {
                return Parity.None;
            }else if (text.Equals("Odd"))
            {
                return Parity.Odd;
            }else if (text.Equals("Even"))
            {
                return Parity.Even;
            }else if (text.Equals("Mark"))
            {
                return Parity.Mark;
            }else if (text.Equals("Space"))
            {
                return Parity.Space;
            }else
            {
                throw new NotImplementedException();
            }
            
        }

        /// <summary>
        /// comboBox可否选择的方法
        /// </summary>
        private void ComboBoxEnabled(Boolean bo)
        {
            //各项comboBox可选择
            comboBox1.Enabled = bo;
            comboBox2.Enabled = bo;
            comboBox3.Enabled = bo;
            comboBox4.Enabled = bo;
            comboBox5.Enabled = bo;
        }

    }
}
